// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARTIFACT4_Artifact4GameMode_generated_h
#error "Artifact4GameMode.generated.h already included, missing '#pragma once' in Artifact4GameMode.h"
#endif
#define ARTIFACT4_Artifact4GameMode_generated_h

#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_RPC_WRAPPERS
#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtifact4GameMode(); \
	friend struct Z_Construct_UClass_AArtifact4GameMode_Statics; \
public: \
	DECLARE_CLASS(AArtifact4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Artifact4"), ARTIFACT4_API) \
	DECLARE_SERIALIZER(AArtifact4GameMode)


#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAArtifact4GameMode(); \
	friend struct Z_Construct_UClass_AArtifact4GameMode_Statics; \
public: \
	DECLARE_CLASS(AArtifact4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/Artifact4"), ARTIFACT4_API) \
	DECLARE_SERIALIZER(AArtifact4GameMode)


#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ARTIFACT4_API AArtifact4GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtifact4GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ARTIFACT4_API, AArtifact4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ARTIFACT4_API AArtifact4GameMode(AArtifact4GameMode&&); \
	ARTIFACT4_API AArtifact4GameMode(const AArtifact4GameMode&); \
public:


#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ARTIFACT4_API AArtifact4GameMode(AArtifact4GameMode&&); \
	ARTIFACT4_API AArtifact4GameMode(const AArtifact4GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ARTIFACT4_API, AArtifact4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArtifact4GameMode)


#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define artifact4_Source_Artifact4_Artifact4GameMode_h_9_PROLOG
#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_RPC_WRAPPERS \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_INCLASS \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact4_Source_Artifact4_Artifact4GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_INCLASS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARTIFACT4_API UClass* StaticClass<class AArtifact4GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact4_Source_Artifact4_Artifact4GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
