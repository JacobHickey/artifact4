// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Artifact4/Artifact4GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeArtifact4GameMode() {}
// Cross Module References
	ARTIFACT4_API UClass* Z_Construct_UClass_AArtifact4GameMode_NoRegister();
	ARTIFACT4_API UClass* Z_Construct_UClass_AArtifact4GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Artifact4();
// End Cross Module References
	void AArtifact4GameMode::StaticRegisterNativesAArtifact4GameMode()
	{
	}
	UClass* Z_Construct_UClass_AArtifact4GameMode_NoRegister()
	{
		return AArtifact4GameMode::StaticClass();
	}
	struct Z_Construct_UClass_AArtifact4GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AArtifact4GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Artifact4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AArtifact4GameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "Artifact4GameMode.h" },
		{ "ModuleRelativePath", "Artifact4GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AArtifact4GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AArtifact4GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AArtifact4GameMode_Statics::ClassParams = {
		&AArtifact4GameMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802A8u,
		METADATA_PARAMS(Z_Construct_UClass_AArtifact4GameMode_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AArtifact4GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AArtifact4GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AArtifact4GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AArtifact4GameMode, 3945636040);
	template<> ARTIFACT4_API UClass* StaticClass<AArtifact4GameMode>()
	{
		return AArtifact4GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AArtifact4GameMode(Z_Construct_UClass_AArtifact4GameMode, &AArtifact4GameMode::StaticClass, TEXT("/Script/Artifact4"), TEXT("AArtifact4GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AArtifact4GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
