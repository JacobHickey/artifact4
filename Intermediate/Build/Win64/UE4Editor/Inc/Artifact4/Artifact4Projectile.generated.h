// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef ARTIFACT4_Artifact4Projectile_generated_h
#error "Artifact4Projectile.generated.h already included, missing '#pragma once' in Artifact4Projectile.h"
#endif
#define ARTIFACT4_Artifact4Projectile_generated_h

#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtifact4Projectile(); \
	friend struct Z_Construct_UClass_AArtifact4Projectile_Statics; \
public: \
	DECLARE_CLASS(AArtifact4Projectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAArtifact4Projectile(); \
	friend struct Z_Construct_UClass_AArtifact4Projectile_Statics; \
public: \
	DECLARE_CLASS(AArtifact4Projectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4Projectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtifact4Projectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtifact4Projectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4Projectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4Projectile(AArtifact4Projectile&&); \
	NO_API AArtifact4Projectile(const AArtifact4Projectile&); \
public:


#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4Projectile(AArtifact4Projectile&&); \
	NO_API AArtifact4Projectile(const AArtifact4Projectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4Projectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4Projectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArtifact4Projectile)


#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AArtifact4Projectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AArtifact4Projectile, ProjectileMovement); }


#define artifact4_Source_Artifact4_Artifact4Projectile_h_9_PROLOG
#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_RPC_WRAPPERS \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_INCLASS \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact4_Source_Artifact4_Artifact4Projectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_INCLASS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4Projectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARTIFACT4_API UClass* StaticClass<class AArtifact4Projectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact4_Source_Artifact4_Artifact4Projectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
