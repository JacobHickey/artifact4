// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARTIFACT4_Artifact4Character_generated_h
#error "Artifact4Character.generated.h already included, missing '#pragma once' in Artifact4Character.h"
#endif
#define ARTIFACT4_Artifact4Character_generated_h

#define artifact4_Source_Artifact4_Artifact4Character_h_14_RPC_WRAPPERS
#define artifact4_Source_Artifact4_Artifact4Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define artifact4_Source_Artifact4_Artifact4Character_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtifact4Character(); \
	friend struct Z_Construct_UClass_AArtifact4Character_Statics; \
public: \
	DECLARE_CLASS(AArtifact4Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4Character)


#define artifact4_Source_Artifact4_Artifact4Character_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAArtifact4Character(); \
	friend struct Z_Construct_UClass_AArtifact4Character_Statics; \
public: \
	DECLARE_CLASS(AArtifact4Character, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4Character)


#define artifact4_Source_Artifact4_Artifact4Character_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtifact4Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtifact4Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4Character(AArtifact4Character&&); \
	NO_API AArtifact4Character(const AArtifact4Character&); \
public:


#define artifact4_Source_Artifact4_Artifact4Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4Character(AArtifact4Character&&); \
	NO_API AArtifact4Character(const AArtifact4Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArtifact4Character)


#define artifact4_Source_Artifact4_Artifact4Character_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(AArtifact4Character, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(AArtifact4Character, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(AArtifact4Character, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(AArtifact4Character, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(AArtifact4Character, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(AArtifact4Character, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(AArtifact4Character, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(AArtifact4Character, L_MotionController); }


#define artifact4_Source_Artifact4_Artifact4Character_h_11_PROLOG
#define artifact4_Source_Artifact4_Artifact4Character_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4Character_h_14_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4Character_h_14_RPC_WRAPPERS \
	artifact4_Source_Artifact4_Artifact4Character_h_14_INCLASS \
	artifact4_Source_Artifact4_Artifact4Character_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact4_Source_Artifact4_Artifact4Character_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4Character_h_14_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4Character_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4Character_h_14_INCLASS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4Character_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARTIFACT4_API UClass* StaticClass<class AArtifact4Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact4_Source_Artifact4_Artifact4Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
