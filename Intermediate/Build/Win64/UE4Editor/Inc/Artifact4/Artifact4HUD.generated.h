// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ARTIFACT4_Artifact4HUD_generated_h
#error "Artifact4HUD.generated.h already included, missing '#pragma once' in Artifact4HUD.h"
#endif
#define ARTIFACT4_Artifact4HUD_generated_h

#define artifact4_Source_Artifact4_Artifact4HUD_h_12_RPC_WRAPPERS
#define artifact4_Source_Artifact4_Artifact4HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define artifact4_Source_Artifact4_Artifact4HUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAArtifact4HUD(); \
	friend struct Z_Construct_UClass_AArtifact4HUD_Statics; \
public: \
	DECLARE_CLASS(AArtifact4HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4HUD)


#define artifact4_Source_Artifact4_Artifact4HUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAArtifact4HUD(); \
	friend struct Z_Construct_UClass_AArtifact4HUD_Statics; \
public: \
	DECLARE_CLASS(AArtifact4HUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Artifact4"), NO_API) \
	DECLARE_SERIALIZER(AArtifact4HUD)


#define artifact4_Source_Artifact4_Artifact4HUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AArtifact4HUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AArtifact4HUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4HUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4HUD(AArtifact4HUD&&); \
	NO_API AArtifact4HUD(const AArtifact4HUD&); \
public:


#define artifact4_Source_Artifact4_Artifact4HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AArtifact4HUD(AArtifact4HUD&&); \
	NO_API AArtifact4HUD(const AArtifact4HUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AArtifact4HUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AArtifact4HUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AArtifact4HUD)


#define artifact4_Source_Artifact4_Artifact4HUD_h_12_PRIVATE_PROPERTY_OFFSET
#define artifact4_Source_Artifact4_Artifact4HUD_h_9_PROLOG
#define artifact4_Source_Artifact4_Artifact4HUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_RPC_WRAPPERS \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_INCLASS \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define artifact4_Source_Artifact4_Artifact4HUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_PRIVATE_PROPERTY_OFFSET \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_INCLASS_NO_PURE_DECLS \
	artifact4_Source_Artifact4_Artifact4HUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ARTIFACT4_API UClass* StaticClass<class AArtifact4HUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID artifact4_Source_Artifact4_Artifact4HUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
