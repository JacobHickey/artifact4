// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Artifact4GameMode.generated.h"

UCLASS(minimalapi)
class AArtifact4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AArtifact4GameMode();
};



