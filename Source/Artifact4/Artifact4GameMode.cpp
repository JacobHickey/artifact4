// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Artifact4GameMode.h"
#include "Artifact4HUD.h"
#include "Artifact4Character.h"
#include "UObject/ConstructorHelpers.h"

AArtifact4GameMode::AArtifact4GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AArtifact4HUD::StaticClass();
}
